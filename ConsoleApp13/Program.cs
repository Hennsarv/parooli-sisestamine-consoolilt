﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    class Program
    {
        internal static string parool = "";
        static void Main(string[] args)
        {
            Console.Write("anna parool: ");
            while (true)
            {
                var k = Console.ReadKey(true);
                if (k.Key == ConsoleKey.Enter) break;
                if (k.Key == ConsoleKey.Backspace)
                {
                    parool = parool.Length == 0 ? "" : parool.Substring(0, parool.Length - 1);
                    Console.Write("\b \b");
                }
                else
                {
                    parool += k.KeyChar.ToString();
                    Console.Write("*");
                }
            }
            Console.WriteLine();
            //Console.WriteLine(parool);
            Northwind22Entities ne = new Northwind22Entities();
            foreach(var e in ne.Employees.Select(x => new { x.FirstName, x.LastName}))
                Console.WriteLine(e);
        }
    }
}
